package com.example.taskonedesign;

import android.os.Bundle;
import android.view.Menu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ItemActivity extends AppCompatActivity {
    List<Product> productList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);

        productList=new ArrayList<>();

        productList.add(new Product("Carrot","₹ 145",R.drawable.carrots));
        productList.add(new Product("Grapes","₹ 400",R.drawable.grapes));
        productList.add(new Product("Watermelom","₹ 40",R.drawable.watermelon));
        productList.add(new Product("Corn","₹ 60",R.drawable.corn));

        RecyclerView myrv = (RecyclerView) findViewById(R.id.recycler_product);
        ItemRecyclerAdapter myAdapter = new ItemRecyclerAdapter(this,productList);
        myrv.setLayoutManager(new GridLayoutManager(this,2));
        myrv.setAdapter(myAdapter);


        Toolbar toolbar = findViewById(R.id.toolbarid);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.item,menu);

        return true;
    }

}


